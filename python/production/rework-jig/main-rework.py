'''
    Author: John Pritchard
    Date: October, 2016

    Description: Main script to control the rework jig. The test jig has one switch, one button, and four LEDs (blue,
                 yellow, green, and red), in addition to an A/D and nRF52 development kit (DK). The A/D acquires signals
                 from the device under test (DUT) and the switches/buttons as well as controls the LEDs. The nRF52 DK
                 flashes the firmware on the nRF52. The jig tests the DUT power rails, electromechanical button, motor
                 driver, and basic BLE functionality. The A/D and DK are connected to an Intel NUC which manages the
                 test scripts and output reports. A CSV file is outputted whenever the switch is turned off (after
                 initially turned on).

    Technical Information:  A/D Model: DATAQ DI-149
                            nRF52 DK: PCA10040 V1.1.0 2016.9

    Usage:  Requires Python 2.7, pyserial, and all JLink utilities including nrfjprog
            Open command prompt, browse to file location, then for example type "python main.py COM15 4800 COM3 19200"
            where COM15 -> A/D com port, 4800 -> A/D baud rate, COM3 -> nRF52 USB-UART com port, 19200 -> nRF52 baud rate

'''

# ----------------------------------------------------------------------------------------------------------------------
# ----- IMPORTS --------------------------------------------------------------------------------------------------------

import logging
import time
import serial
import sys
import csv
from time import sleep
import subprocess

# ----------------------------------------------------------------------------------------------------------------------
# ----- CLASSES --------------------------------------------------------------------------------------------------------

class Dataq:

    def getDin(self, ser):
        # ----------- define variables -----------
        _list = []                      # blank list array to fill with buffer

        # ----------- set up streaming data -----------
        ser.flushInput()
        ser.write('\rstop\r')
        # "float" --> data type; "slist [D] [xH]" --> D is position in scanning list, H is channel number to scan
        ser.write('\r'.join(['', 'float', 'slist 0 x8', '']))       # look at digital input only (button pressing)
        ser.write('start\r')                                        # start scanning
        sleep(0.1)                                                  # allow setup time for 0.1 sec

        # ----------- get ~3 sets of readings and parse the data -----------
        ser.flushInput()                # clear the input
        buf = ser.read(16)              # read in 16 bytes, or about 3x 4-chan scans, to buffer
        ser.write('\rstop\r')           # stop the scan
        _list.append(buf)               # append buffer to blank list
        tmp = self.parse(_list, 2)      # parse the data, returning fully parsed option (opt 2)

        din_val = int(tmp[1])           # set din_val as int form of the string found in the 2nd element of tmp

        return din_val

    def getSwitchStatus(self, ser):

        swVal = self.getDin(ser)

        if swVal == 11 or swVal == 10:  # account for switch on and button pressed simultaneously
            return 1
        else:
            return 0

    def setDout(self, ser, dchan):

        if dchan == 0:
            ser.write('\rD0F\r')    # clear LEDs

        if dchan == 1:
            ser.write('\rD0E\r')    # set LED channel 1

        if dchan == 2:
            ser.write('\rD0D\r')    # set LED channel 2

        if dchan == 3:
            ser.write('\rD0B\r')    # set LED channel 3

        if dchan == 4:
            ser.write('\rD07\r')    # set LED channel 4

        return

    def parse(self, dat, opt):

        # split the state by carriage returns, then remove the first and last element if they are not complete
        lines = ''.join(dat).split('\r')

        if not lines[0].startswith('sc'):  # account for possible partial measurement output from first element
            lines = lines[1:]  # remove partial data if necessary
        lines.pop()  # account for possible partial measurement output from last element
        tmp = lines[0].split(' ')  # split by space ' '
        tmp = filter(None, tmp)  # remove any empty elements

        # allow option to return mostly parsed lines, or fully parsed (tmp variable)
        # this is useful in the getAin1 function
        if opt == 1:
            return lines
        if opt == 2:
            return tmp
        else:
            return -1

    def getTestButtonStatus(self, ser):
        but_val = self.getDin(ser)
        if but_val == 10:
            return 1
        else:
            return 0

    def getAin1(self, ser):
        # ----------- define variables -----------
        _list = []      # blank list array to fill with buffer
        _chan1_tot = 0  # chanX_tot is a summation variable used to calculate average voltage at chanX
        _chan2_tot = 0
        _chan3_tot = 0
        _chan4_tot = 0

        # ----------- set up streaming data -----------
        ser.flushInput()
        ser.write('\rstop\r')
        # "float" --> data type; "slist [D] [xH]" --> D is position in scanning list, H is channel number to scan
        ser.write('\r'.join(['', 'float', 'slist 0 x0', 'slist 1 x1', 'slist 2 x2', 'slist 3 x3', '']))
        ser.write('start\r')
        sleep(0.1)

        # ----------- get ~50 sets of readings -----------
        ser.flushInput()
        buf = ser.read(1600)            # read in 1600 bytes, or about 50 4-chan scans, to buffer
        ser.write('\rstop\r')           # stop the scan
        _list.append(buf)               # append buffer to blank list
        lines = self.parse(_list, 1)    # parse the data, returning mostly parsed option (opt 1)
        tot_lines = len(lines)

        # ----------- get mean of sets of readings -----------
        for i in range(0, tot_lines):
            tmp = lines[i].split(' ')  # split by space ' '
            tmp = filter(None, tmp)  # remove any empty elements
            _chan1_tot += float(tmp[1])  # continue to sum the measurements from each channel
            _chan2_tot += float(tmp[2])
            _chan3_tot += float(tmp[3])
            _chan4_tot += float(tmp[4])

        chan1234_mean = [_chan1_tot / tot_lines, _chan2_tot / tot_lines, _chan3_tot / tot_lines, _chan4_tot / tot_lines]

        return chan1234_mean

    def getAin2(self, ser):
        # ----------- define variables -----------
        _list = []      # blank list array to fill with buffer
        _chan1_tot = 0  # chanX_tot is a summation variable used to calculate average voltage at chanX
        _chan2_tot = 0

        # ----------- set up streaming data -----------
        ser.flushInput()
        ser.write('\rstop\r')
        # "float" --> data type; "slist [D] [xH]" --> D is position in scanning list, H is channel number to scan
        ser.write('\r'.join(['', 'float', 'slist 0 x4', 'slist 1 x5', '']))
        ser.write('start\r')
        sleep(0.1)

        # ----------- get ~50 sets of readings -----------
        ser.flushInput()
        buf = ser.read(1600)              # read in 1600 bytes, or about 50 4-chan scans, to buffer
        ser.write('\rstop\r')           # stop the scan
        _list.append(buf)               # append buffer to blank list
        lines = self.parse(_list, 1)    # parse the data, returning mostly parsed option (opt 1)
        tot_lines = len(lines)

        # ----------- get mean of sets of readings -----------
        for i in range(0, tot_lines):
            tmp = lines[i].split(' ')       # split by space ' '
            tmp = filter(None, tmp)         # remove any empty elements
            _chan1_tot += float(tmp[1])     # continue to sum the measurements from each channel
            _chan2_tot += float(tmp[2])

        chan12_mean = [_chan1_tot / tot_lines, _chan2_tot / tot_lines]

        return chan12_mean

class HabitAware:

    def pcbLoadedCheck(self, ser):

        # set up streaming data
        ser.flushInput()
        ser.write('\rstop\r')
        # "float" --> data type; "slist [D] [xH]" --> D is position in scanning list, H is channel number to scan
        ser.write('\r'.join(['', 'float', 'slist 0 x6', '']))  # look at digital input only (button pressing)
        ser.write('start\r')  # start scanning
        sleep(0.1)

        # get data, parse it
        _state = []  # reset state array
        ser.flushInput()  # clear the input
        buf = ser.read(32)  # read in 16 bytes, or about 3x 4-chan scans, to buffer
        ser.write('\rstop\r')  # stop the scan
        _state.append(buf)  # append buffer to blank list
        tmp = Dataq().parse(_state, 2)

        # check if the pcb was removed
        if float(tmp[1]) > 3.3:  # if pcb loaded
            return 1
        else:
            return 0

    def pwrCheck(self, meas):

        # initialize error variables
        nrf_error = 0
        v33_error = 0
        batt_error = 0

        # check voltage levels
        if meas[0] < 3.135 or meas[0] > 3.47:  # if 3.3V rail outside 3.3V +/- 5%
            v33_error = 1  # then this is a big issue, possibly pointing to the regulator?
        if meas[1] < 3.135 or meas[1] > 3.47:  # if nRF rail outside 3.3V +/- 5%
            nrf_error = 1  # then the nRF and other peripherals on that rail not getting power - probably an issue with the pMOSFET
        if meas[2] < 2.5 or meas[2] > 3.12:  # if voltage at battery unexpected
            batt_error = 1  # then charger circuit not outputting right open-circuit voltage at Vbatt

        return [nrf_error, v33_error, batt_error]

    def btnCheck(self, ser):
        # NOTE: In testing, we notice if we see ~1.6 V on the button but the rails are good, the pull-down resistor is
        # not soldered properly

        flag = 0
        # set up streaming data
        ser.flushInput()
        ser.write('\rstop\r')
        # "float" --> data type; "slist [D] [xH]" --> D is position in scanning list, H is channel number to scan
        ser.write('\r'.join(['', 'float', 'slist 0 x3', '']))   # look at digital input only (button pressing)
        ser.write('start\r')                                    # start scanning
        sleep(0.1)

        # parse the data then check if the button was pressed
        temp_then = time.time()
        while not flag:
            # get data, then parse it
            _list = []                      # blank list array to fill with buffer
            ser.flushInput()                # clear the input
            buf = ser.read(32)              # read in 16 bytes, or about 3x 4-chan scans, to buffer
            _list.append(buf)               # append buffer to blank list
            tmp = Dataq().parse(_list, 2)

            # check if the button was pressed
            if float(tmp[1]) > 1.5:         # if button pressed
                ser.write('\rstop\r')       # stop the scan
                flag = 1
                err = 0

            if time.time() - temp_then > 5:
                ser.write('\rstop\r')       # stop the scan
                flag = 1
                err = 1                     # indicate button error, since timeout occurred
                habitaware.log('    ...electromechanical timeout.', 'error')

        return err

    def internalCheck(self, ser_nrf52, ser_dataq):

        # err[1] -> batt, err[2] -> imu, err[3] -> rtc, err[4] -> motor
        err = [1, 1, 1, 1]
        habitaware.log('    Getting battery level...', 'info')
        batt_test_result = self.battCheck(ser_nrf52)
        err[0] = batt_test_result[0]
        habitaware.log('    Running IMU test...', 'info')
        imu_test_result = self.imuCheck(ser_nrf52)
        err[1] = imu_test_result[0]
        habitaware.log('    Running RTC test...', 'info')
        rtc_test_result = self.rtcCheck(ser_nrf52)
        err[2] = rtc_test_result[0]
        habitaware.log('    Running motor test...', 'info')
        mtr_test_result = self.mtrCheck(ser_nrf52, ser_dataq)
        err[3] = mtr_test_result[0]

        # compile into one return variable
        internal_test_results = [err, batt_test_result[1:], imu_test_result[1:], rtc_test_result[1:], mtr_test_result[1:]]
        return internal_test_results

    def buildTestReport(self, time_stamp, serial_num, analog_data, power_err, button_err, program_err, internal_data):
        # define note based on order of important errors (greatest to least)
        note = ''
        err = 0
        if sum(power_err) > 0:
            note += '01 Power error, check rails. '
            err += 1
        if program_err:
            note += '02 Did not program, check USB connection. '
            err += 1
        if sum(internal_data[0]) > 0:
            if internal_data[0][3]:
                note += '03 Motor test failure. '
            if internal_data[0][1]:
                note += '04 IMU self-test failure. '
            if internal_data[0][2]:
                note += '05 RTC time-test failure. '
            if internal_data[0][0]:
                note += '06 Battery measurement failure. '
            err += 1
        if button_err:
            note += '07 Button did not register, please rework button. '
            err += 1
        if err > 0:
            pass_fail = 'Fail'
        else:
            pass_fail = 'Pass'
            note = 'ALL PASS'

        report = [time_stamp, serial_num, analog_data[0], analog_data[1], analog_data[2], analog_data[3],
                  internal_data[4][:], internal_data[1][0], internal_data[2][0], internal_data[3][0], pass_fail, note]

        return report

    def program(self, cmd_file):

        process = subprocess.Popen(cmd_file, shell=True, stdout=subprocess.PIPE)
        process.wait()
        if process.returncode != 0:
            habitaware.log("Error during flashing.", 'error')
            return 0
        else:
            habitaware.log("Flashing completed successfully.", 'info')
            return 1

    def rtcCheck(self, ser):
        val1 = nrf52.rtc(ser)   # get time
        sleep(1)                # wait 1 seconds
        val2 = nrf52.rtc(ser)   # get time
        delta = val2 - val1     # get the delta

        # delta should be between 1 & 2 seconds
        if delta <= 2 and delta > 0:
            return [0, delta]
        else:
            return [1, delta]

    def battCheck(self, ser):
        # get batt level and ensure between 90 - 100 (nominal 95)
        battLevel = nrf52.battery(ser)

        # if comms good, report batt level test, o/w return error
        if battLevel >= 90 and battLevel <= 100:
            return [0, battLevel]
        else:
            return [1, battLevel]

    def imuCheck(self, ser):
        # check the imu test return value, 7 is a pass
        imuVal = nrf52.imu(ser)  # run the IMU test

        # if comms good, report imu test, o/w return error
        if imuVal == 7:
            return [0, imuVal]
        else:
            return [1, imuVal]

    def mtrCheck(self, ser1, ser2):
        # ----------- test motor I/O -----------
        habitaware.log('    Testing motor...', 'info')

        # measure value with motor off
        mtrState = nRF52().motor(ser1,'off')
        if mtrState == 'Motor off':
            habitaware.log('        received motor off.', 'info')
            mtrVal1 = Dataq().getAin2(ser2)
        else:
            habitaware.log('        ERROR, could not communicate with nRF52.', 'error')

        # measure value with motor on
        mtrState = nRF52().motor(ser1, 'on')
        if mtrState == 'Motor on':
            habitaware.log('        received motor on.', 'info')
            mtrVal2 = Dataq().getAin2(ser2)
        else:
            habitaware.log('        ERROR, could not communicate with nRF52.', 'error')

        # measure value with motor off
        mtrState = nRF52().motor(ser1, 'off')
        if mtrState == 'Motor off':
            habitaware.log('        received motor off.', 'info')
            mtrVal3 = Dataq().getAin2(ser2)
            continue_test = 1
        else:
            habitaware.log('        ERROR, could not communicate with nRF52.', 'error')
            continue_test = 0

        # assuming no errors occured, return the difference of the toggled states, o/w return error
        if continue_test == 1:
            # check against pass/fail conditions
            ioToggle1 = mtrVal2[0] - mtrVal1[0]     # expecting +3.3 +/- 5% (I/O off: 0 V, I/O on: 3.3 V)
            vdsToggle1 = mtrVal2[1] - mtrVal1[1]    # expecting -3.3 +/- 5% (VDS off: 3.3 V, VDS on: 0 V)
            ioToggle2 = mtrVal3[0] - mtrVal2[0]     # expecting -3.3 +/- 5%
            vdsToggle2 = mtrVal3[1] - mtrVal2[1]    # expecting +3.3 +/- 5%
            # print([ioToggle1, vdsToggle1, ioToggle2, vdsToggle2])

            # TODO - remove this, we are faking the motor pass for now
            # return [0, ioToggle1, vdsToggle1, ioToggle2, vdsToggle2]

            # TODO - enable this once we get a good jig with proper motor contact pogo-pins
            if 3.135 < ioToggle1 <= 3.3 and -3.3 <= ioToggle2 < -3.135 and -4.41 <= vdsToggle1 < -3.99 and 3.99 < vdsToggle2 <= 4.41:
                return [0, ioToggle1, vdsToggle1, ioToggle2, vdsToggle2]
            else:
                return [1, ioToggle1, vdsToggle1, ioToggle2, vdsToggle2]
        else:
            return [1, 0, 0, 0, 0]

    def log(self, msg, type):

        # print the message to screen, then ad to log file
        print(msg)

        if type == 'info':
            logging.info(msg)
        elif type == 'error':
            logging.error(msg)
        elif type == 'warning':
            logging.warning(msg)
        else:
            pass

class nRF52:

    def led(self, ser, state):

        # check the state, reassign it according to nRF52 comm definitions
        if state == 'on':
            val = '1'
        elif state == 'off':
            val = '2'
        else:
            val = '2'

        # write LED state to the nRF52
        ser.flushInput()
        ser.flushOutput()
        ser.write(val)

        # read nRF52 response
        uartIn = ser.readline()
        uartIn = uartIn[:-2]    # strip the space and new-line character
        if uartIn:
            habitaware.log('    ' + uartIn, 'info')
            return uartIn
        else:
            habitaware.log('Error, could not communicate with nRF52.' + uartIn, 'error')
            return 0

    def imu(self, ser):

        # write '0' to nRF to start IMU test
        ser.flushInput()
        ser.flushOutput()
        ser.write('0')

        # read nRF52 response
        uartIn = ser.readline()
        uartIn = uartIn[:-2]    # strip the space and new-line character

        # ensure string valid, or report error
        if uartIn:
            try:
                return int(uartIn[22:])       # print the last item, which is the result
            except:
                habitaware.log('Error, invalid string' + uartIn[22:], 'error')
                return 0
        else:
            habitaware.log('Error, invalid string' + uartIn[22:], 'error')
            return 0

    def motor(self, ser, state):

        # check the state, reassign it according to nRF52 comm definitions
        if state == 'on':
            val = '3'
        elif state == 'off':
            val = '4'
        else:
            habitaware.log('Error, unknown motor state received from nRF52: ' + state, 'error')
            val = '4'

        # write motor state to the nRF52
        ser.flushInput()
        ser.flushOutput()
        ser.write(val)

        # read nRF52 response
        uartIn = ser.readline()
        uartIn = uartIn[:-2]  # strip the space and new-line character
        return uartIn

    def rtc(self, ser):

        # write '5' to nRF to start RTC test
        ser.flushInput()
        ser.flushOutput()
        ser.write('5')

        # read nRF52 response
        uartIn = ser.readline()
        uartIn = uartIn[:-2]    # strip the space and new-line character

        # ensure comms are good, o/w return 0
        if uartIn:
            try:
                return int(uartIn[11:]) # return the last item, which is the result
            except:
                habitaware.log('Error, invalid string' + uartIn[11:], 'error')
                return 0
        else:
            habitaware.log('Error, invalid string' + uartIn[11:], 'error')
            return 0                # if no response return a 0

    def mac(self, ser):

        # write '7' to nRF to get MAC address
        ser.flushInput()
        ser.flushOutput()
        ser.write('7')

        # read nRF52 response
        uartIn = ser.readline()
        uartIn = uartIn[:-2]                # strip the space and new-line character

        # ensure proper MAC received, o/w return error
        if uartIn:
            try:
                return uartIn[11:]
            except:
                habitaware.log('Error, invalid string' + uartIn[11:], 'error')
                return 'Error, could not communicate with nRF52.'
        else:
            return 'Error, could not communicate with nRF52.'

    def battery(self, ser):

        # write '6' to nRF to get the battery %
        ser.flushInput()
        ser.flushOutput()
        ser.write('6')

        # read nRF52 response
        uartIn = ser.readline()
        uartIn = uartIn[:-2]    # strip the space and new-line character
        if uartIn:
            try:
                return int(uartIn[9:])       # return the number only
            except:
                habitaware.log('Error, invalid string' + uartIn[9:], 'error')
                return 0
        else:
            return 0

# ----------------------------------------------------------------------------------------------------------------------
# ----- MAIN SCRIPT ----------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------
# ---------- Initialize and Connect to com port peripherals ----------

# define classes
dataq = Dataq()
habitaware = HabitAware()
nrf52 = nRF52()

# generate log file
file_name = time.asctime(time.localtime(time.time()))               # create unique filename (time-based)
file_name = file_name.replace(':', '.')                             # filename cannot have special characters
log_file_name = file_name + '.log'                                  # add serial number to filename
logging.basicConfig(filename=log_file_name, level=logging.DEBUG)    # start logging for this file

# try to connect to the serial ports, indicate user error if not connecting
com_port_1 = sys.argv[1]
baud_rate_1 = sys.argv[2]
com_port_2 = sys.argv[3]
baud_rate_2 = sys.argv[4]

habitaware.log("Connecting...", 'info')
try:
    ser_dataq = serial.Serial(com_port_1, baud_rate_1, timeout=1)
    habitaware.log("Connected to DATAQ A/D Converter.", 'info')
    try:
        ser_nrf52 = serial.Serial(com_port_2, baud_rate_2, timeout=2)
        habitaware.log("Connected to USB-UART cable.", 'info')
    except:
        ser_dataq.close()  # close the just-opened port, since we will be ending this function early
        habitaware.log("Input Error, Invalid COM input", 'error')

except:
    habitaware.log("Input Error, Invalid COM input", 'error')
    exit()

# clear indicator LEDs
dataq.setDout(ser_dataq, 0)

# -----------------------------------------------------
# ---------- Run main testing loop --------------------

while True:

    # Constantly check for switch being turned on. When turned on, move on in code
    habitaware.log('Checking switch...', 'info')
    while dataq.getSwitchStatus(ser_dataq) == 0:
        pass

    # Create new test file with time and date
    habitaware.log("...switch turned on.", 'info')
    file_name = time.asctime(time.localtime(time.time()))   # create unique filename (time-based)
    file_name = file_name.replace(':', '.')                 # filename cannot have special characters

    # open csv file and write header row
    with open(file_name + '.csv', 'wb') as test_file:
        file_writer = csv.writer(test_file, lineterminator='\n')
        file_writer.writerow(
            ["Time", "MAC", "3v3 Rail (V)", "nRF VDD (V)", "Batt Analog (V)",
             "Idle Button (V)", "Motor Test", "Batt Digital", "IMU Test",
             "RTC Test", "Pass/Fail", "Notes"])

    # -----------------------------------------------------
    # ---------- Every time switch toggles on -------------

    # Initialize counters/flags
    dut_count = 0

    # loop while the switch remains "ON"
    while dataq.getSwitchStatus(ser_dataq) == 1:

        habitaware.log("Waiting for PCB to be loaded...", 'info')
        then = time.time()
        temp_flag = 0

        # blink 'ready' LED indicating power not plugged in
        while temp_flag == 0:
            if habitaware.pcbLoadedCheck(ser_dataq) == 0:
                if time.time() - then > 0.3:
                    dataq.setDout(ser_dataq, 1)  # set 'ready' LED
                if time.time() - then > 0.7:
                    dataq.setDout(ser_dataq, 0)  # clear 'ready' LED
                    then = time.time()
            else:
                habitaware.log("...PCB Loaded.", 'info')
                temp_flag = 1
            if dataq.getSwitchStatus(ser_dataq) == 0:
                temp_flag = 1

        # solid 'ready' LED
        dataq.setDout(ser_dataq, 1)

        # -----------------------------------------------------
        # ---------- DUT Test Loop ----------------------------
        if dataq.getSwitchStatus(ser_dataq) == 1:   # condition to ensure switch still on
            habitaware.log('Waiting for Start Test button press...', 'info')
            while dataq.getTestButtonStatus(ser_dataq) == 0:
                pass

            # ----------- every time button pressed, run test sequence -----------
            habitaware.log("...button pressed.", 'info')
            dataq.setDout(ser_dataq, 0)                         # clear LEDs, indicating jig is 'thinking'
            sleep(0.1)                                          # allow 0.5 sec for debounce
            dut_count += 1                                      # update DUT count
            now = time.asctime(time.localtime(time.time()))     # create unique filename (time-based)
            now = file_name.replace(':', '.')                   # filename cannot have special characters
            habitaware.log('----------- Testing DUT_' + str(dut_count) + ' ' + now + ' -----------', 'info')

            # When "Start Test" button pressed, begin all tests and firmware flashing
                # Pull the DUT serial number
                # Run electrical tests & electromechanical button test
                # If all of the above pass - Upload production firmware and use DK to connect and trigger motor

            habitaware.log('Starting test...', 'info')

            # ----------- get analog input, error check it -----------
            habitaware.log('Obtaining analog input...', 'info')
            analog_data = dataq.getAin1(ser_dataq)
            pwr_test_results = habitaware.pwrCheck(analog_data)
            #print analog_data
            #print pwr_test_results

            # ----------- flash the test firmware -----------
            if sum(pwr_test_results[0:2]) > 0:
                habitaware.log('FAIL 01 Power rails too low, NOT OK to flash test firmware', 'error')
                dataq.setDout(ser_dataq, 4)     # set 'board is bad' LED
                pgm_test_results = 1

                # update file with known information, all un-run test labeled 'XXXX'
                with open(file_name + '.csv', 'a') as test_file:
                    file_writer = csv.writer(test_file, lineterminator='\n')
                    file_writer.writerow(
                        [time.time(), 'XXXX', analog_data[0], analog_data[1], analog_data[2], analog_data[3], 'XXXX',
                         'XXXX', 'XXXX', 'XXXX', 'Fail','Power rails too low, NOT OK to flash test firmware' ])
                habitaware.log('Test complete.', 'info')
                habitaware.log('--------------------------------------------------------------', 'info')
            else:
                habitaware.log('Flashing test firmware...', 'info')
                if habitaware.program('program_ha_prod_test_firmware.cmd') != 1:
                    habitaware.log('Flashing unsuccessful', 'error')
                    dataq.setDout(ser_dataq, 4)  # set 'board is bad' LED
                    pgm_test_results = 1

                    # update file with known information, all un-run test labeled 'XXXX'
                    habitaware.log('Updating test file...', 'info')

                    with open(file_name + '.csv', 'a') as test_file:
                        file_writer = csv.writer(test_file, lineterminator='\n')
                        file_writer.writerow(
                            [time.time(), 'XXXX', analog_data[0], analog_data[1], analog_data[2], analog_data[3],
                             'XXXX', 'XXXX', 'XXXX', 'XXXX', 'Fail', 'Flashing unsuccessful'])
                    habitaware.log('Test complete.', 'info')
                    habitaware.log('--------------------------------------------------------------', 'info')
                else:
                    pgm_test_results = 0
                    for i in range(1, 3):           # TODO - come up with better way to clear buffer
                        ser_nrf52.readline()

                    # ----------- test the battery, IMU, motor, and RTC -----------
                    habitaware.log('Testing digital components...', 'info')
                    int_error = habitaware.internalCheck(ser_nrf52, ser_dataq)

                    # TODO ----------- DK connects to the DUT with same MAC address via BLE -----------
                    habitaware.log('    Getting MAC address...', 'info')
                    serial_num = nrf52.mac(ser_nrf52)   # obtain the MAC address

                    # ----------- wait for electromechanical button press using the on-board LED as a trigger -----------
                    nrf52.led(ser_nrf52, 'on')                              # turn on the on-board LED
                    habitaware.log('    Waiting for electromechanical button press...', 'info')
                    dataq.setDout(ser_dataq, 2)                             # set 'press electromechanical button' LED
                    btn_test_results = habitaware.btnCheck(ser_dataq)              # check the button (w/ timeout)
                    dataq.setDout(ser_dataq, 0)                             # clear LEDs, indicating jig is 'thinking'
                    nrf52.led(ser_nrf52, 'off')                             # turn off the on-board LED

                    # ----------- flash production firmware -----------
                    if pgm_test_results == 0:
                        habitaware.log('Uploading final production firmware...', 'info')
                        if habitaware.program('program_ha_prod_final_firmware.cmd') != 1:
                            pgm_test_results = 1
                        else:
                            pass

                    # ----------- update test file and jig LEDs -----------
                    habitaware.log('Indicating PASS/FAIL to the user...', 'info')
                    all_test_results = habitaware.buildTestReport(time.time(), serial_num, analog_data, pwr_test_results, btn_test_results, pgm_test_results,
                                                      int_error)

                    # TODO - Update Test File - if fail, PRINT A STICKER
                    habitaware.log('Updating test file...', 'info')
                    with open(file_name + '.csv', 'a') as test_file:
                        file_writer = csv.writer(test_file, lineterminator='\n')
                        file_writer.writerow(all_test_results)

                    # log and set output
                    habitaware.log('...test finished.', 'info')
                    if all_test_results[10] == 'Pass':
                        dataq.setDout(ser_dataq, 3)  # set 'board is good' LED
                        habitaware.log(all_test_results[-1], 'info')    # this is the pass note
                    else:
                        dataq.setDout(ser_dataq, 4)  # set 'board is bad' LED
                        habitaware.log('FAIL ' + all_test_results[-1], 'error')  # this is the fail note
                    habitaware.log('--------------------------------------------------------------', 'info')


            # ----------- wait for pcb to be unloaded or switch turned off -----------
            habitaware.log('Waiting for PCB to be unloaded or switch turned off...', 'info')
            temp_flag = 0
            while temp_flag == 0:
                if dataq.getSwitchStatus(ser_dataq) == 0:
                    habitaware.log('...switch turned off.', 'info')
                    temp_flag = 1
                if habitaware.pcbLoadedCheck(ser_dataq) == 0:
                    habitaware.log('...PCB unloaded.', 'info')
                    temp_flag = 1

        # <---GOTO LOOP 1--->

        # When switch is turned off, finalize CSV file if needed but keep session log open


# ----------------------------------------------------------------------------------------------------------------------
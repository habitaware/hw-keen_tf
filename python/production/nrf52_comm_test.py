
# ---------- imports ----------
import serial
import sys
import subprocess
import time
from pynrfjprog import API, Hex

# ---------- nrf52 class ----------
class nRF52:

    def led(self, ser, state):
        if state == 'on':
            val = '1'
        elif state == 'off':
            val = '2'
        else:
            val = '2'

        print('writing to nRF52..')
        ser.flushInput()
        ser.flushOutput()
        ser.write(val)
        time.sleep(0.1)

        print('reading from nRF52:')
        uartIn = ser.readline()
        uartIn = uartIn[:-1]
        print uartIn

# ---------- HabitAware class ----------
class HabitAware:

    def program(self, cmd_file):

        process = subprocess.Popen(cmd_file, shell=True, stdout=subprocess.PIPE)
        process.wait()
        if process.returncode != 0:
            print "Error during flashing."
            return 0
        else:
            print "Flashing completed successfully."
            return 1


# ---------- main script ----------

# set variables/objects
com_port_2 = sys.argv[1]
baud_rate_2 = sys.argv[2]
nrf52 = nRF52()
habitaware = HabitAware()

# connect to the cable
try:
    ser_nrf52 = serial.Serial(com_port_2, baud_rate_2, timeout=1)
    print("Connected to USB-UART cable.")
except:
    print("Input Error, Invalid COM input")

# program DUT with test firmware
print('Flashing...')
habitaware.program('program_ha_prod_test_firmware.cmd')

# get rid of the header by clearing buffer (using readline())
for i in range(1, 3):  # TODO - come up with better way to clear buffer
    ser_nrf52.readline()

# try class/function
print('Turning on LED...')
nrf52.led(ser_nrf52,'on')
print('Waiting one second...')
time.sleep(1)
print('Turning off LED...')
nrf52.led(ser_nrf52,'off')
print('Test complete.')